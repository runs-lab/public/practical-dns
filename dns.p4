#include <core.p4>
#include <tna.p4>

#define ETHERTYPE_IPV4 0x800
#define UDP_PROTOCOL 0x11
#define CACHE_PREFIX 16w0xc00c


/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/


typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;

struct dns_cache_sig {
    bit<32>     ts;
    bit<32>     sig;
}

header ethernet_t {
    macAddr_t dstAddr;
    macAddr_t srcAddr;
    bit<16>   etherType;
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<8>    diffserv;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
}

header ipv4_val {
    bit<32> ip_s;
    bit<32> ip_d;
}

header udp_ports {
    bit<16> sport;
    bit<16> dport;
}

header udp_h {
    bit<16> len;
    bit<16> chksum; 
}

header dns_h {
    bit<16> id;
    bit<1> qr;
    bit<4> opcode;
    bit<1> auth_answer;
    bit<1> truncation;
    bit<1> rec_desired;
    bit<1> rec_avail;
    bit<3> reserved;
    bit<4> resp_code;
    bit<16> qdcount;
    bit<16> ancount;
    bit<16> nscount;
    bit<16> arcount;
}

header dns_e {
    bit<8>  lenstop;
    bit<16> qtype;
    bit<16> qclass;
}

header dns_a {
    bit<16> pointer;
    bit<16> type;
    bit<16> clas;
    bit<32> ttl;
    bit<16> rdlength;
}

header dns_a_r {
    bit<32> res;
}

header dns_char {
    bit<8>   c;
}

header dns_len {
    bit<8>   l;
}


header dns_label_1 {
    bit<8>  l;
}

header dns_label_2 {
    bit<16>  l;
}

header dns_label_4 {
    bit<32>  l;
}

header dns_label_8 {
    bit<64>  l;
}

header dns_label_16 {
    bit<128>  l;
}


struct ig_metadata {
}

struct eg_metadata {
}


struct headers {
    ethernet_t                          ethernet;
    ipv4_t                              ipv4;
    ipv4_val                            ipv4v;
    ipv4_val                            ipv4v_alt;
    udp_ports                           udp_p;
    udp_ports                           udp_p_alt;
    udp_h                               udp;
    dns_h                               dns;

    
    dns_len                             len1;
    dns_label_16                     label1_16;
    dns_label_8                     label1_8;
    dns_label_4                     label1_4;
    dns_label_2                     label1_2;
    dns_label_1                     label1_1;
    dns_len                             len2;
    dns_label_16                     label2_16;
    dns_label_8                     label2_8;
    dns_label_4                     label2_4;
    dns_label_2                     label2_2;
    dns_label_1                     label2_1;
    dns_len                             len3;
    dns_label_16                     label3_16;
    dns_label_8                     label3_8;
    dns_label_4                     label3_4;
    dns_label_2                     label3_2;
    dns_label_1                     label3_1;
    dns_len                             len4;
    dns_label_16                     label4_16;
    dns_label_8                     label4_8;
    dns_label_4                     label4_4;
    dns_label_2                     label4_2;
    dns_label_1                     label4_1;
    dns_len                             len5;
    dns_label_16                     label5_16;
    dns_label_8                     label5_8;
    dns_label_4                     label5_4;
    dns_label_2                     label5_2;
    dns_label_1                     label5_1;
    dns_len                             len6;
    dns_label_16                     label6_16;
    dns_label_8                     label6_8;
    dns_label_4                     label6_4;
    dns_label_2                     label6_2;
    dns_label_1                     label6_1;

    dns_e                               dns_extra;
    dns_a                               dns_answer;
    dns_a_r                             dns_answer_ip;
}

/*************************************************************************
*********************** I N G - P A R S E R  *****************************
*************************************************************************/


parser TofinoIngressParser(
    packet_in pkt,
    out ingress_intrinsic_metadata_t ig_intr_md
) {
    state start {
        pkt.extract(ig_intr_md);
        transition select(ig_intr_md.resubmit_flag) {
            1 : parse_resubmit;
            0 : parse_port_metadata;
        }
    }

    state parse_resubmit {
        // Parse resubmitted packet here.
        transition reject;
    }

    state parse_port_metadata {
        pkt.advance(PORT_METADATA_SIZE);
        transition accept;
    }
}

parser SwitchIngressParser(
    packet_in pkt,
    out headers hdr,
    out ig_metadata meta,
    out ingress_intrinsic_metadata_t ig_intr_md
) {
    ParserCounter()         len_limit;
    TofinoIngressParser() tofino_parser;


    state start {
        tofino_parser.apply(pkt, ig_intr_md);
        transition parse_ethernet;
    }

    
    state parse_ethernet {
        pkt.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            ETHERTYPE_IPV4 :    parse_ipv4;
            default :           accept;
        }
    }

    state parse_ipv4 {
        pkt.extract(hdr.ipv4);
        pkt.extract(hdr.ipv4v);
        transition select(hdr.ipv4.protocol) {
            UDP_PROTOCOL: parse_udp;
            default :   accept;
        }
    }

    state parse_udp {
        pkt.extract(hdr.udp_p);
        pkt.extract(hdr.udp);
        transition select(hdr.udp_p.dport,hdr.udp_p.sport) {
            (_,53) :      parse_dns_h;
            (53,_) :      parse_dns_h;
            default :   accept;
        }
    }

    state parse_dns_h {
        pkt.extract(hdr.dns);
        len_limit.set(8w60);

        transition parse_len1;
    }

    
    state parse_len1 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label1;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len2 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label2;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len3 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label3;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len4 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label4;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len5 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label5;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len6 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label6;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len7 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    

    
    state preparse_label1 {
        pkt.extract(hdr.len1);
        
        transition select(hdr.len1.l) { 
            16 &&& 16:          parse_label1_16;
            8 &&& 8:          parse_label1_8;
            4 &&& 4:          parse_label1_4;
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
        }
    }

    state postparse_label1 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len2;
            true:                     accept;
        }
    }
    
    state parse_label1_16 {
        pkt.extract(hdr.label1_16);
        len_limit.decrement(8w16);

        transition select(hdr.len1.l) { 
            8 &&& 8:          parse_label1_8;
            4 &&& 4:          parse_label1_4;
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_8 {
        pkt.extract(hdr.label1_8);
        len_limit.decrement(8w8);

        transition select(hdr.len1.l) { 
            4 &&& 4:          parse_label1_4;
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_4 {
        pkt.extract(hdr.label1_4);
        len_limit.decrement(8w4);

        transition select(hdr.len1.l) { 
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_2 {
        pkt.extract(hdr.label1_2);
        len_limit.decrement(8w2);

        transition select(hdr.len1.l) { 
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_1 {
        pkt.extract(hdr.label1_1);
        len_limit.decrement(8w1);

        transition postparse_label1;
    }
    
    state preparse_label2 {
        pkt.extract(hdr.len2);
        
        transition select(hdr.len2.l) { 
            16 &&& 16:          parse_label2_16;
            8 &&& 8:          parse_label2_8;
            4 &&& 4:          parse_label2_4;
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
        }
    }

    state postparse_label2 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len3;
            true:                     accept;
        }
    }
    
    state parse_label2_16 {
        pkt.extract(hdr.label2_16);
        len_limit.decrement(8w16);

        transition select(hdr.len2.l) { 
            8 &&& 8:          parse_label2_8;
            4 &&& 4:          parse_label2_4;
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_8 {
        pkt.extract(hdr.label2_8);
        len_limit.decrement(8w8);

        transition select(hdr.len2.l) { 
            4 &&& 4:          parse_label2_4;
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_4 {
        pkt.extract(hdr.label2_4);
        len_limit.decrement(8w4);

        transition select(hdr.len2.l) { 
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_2 {
        pkt.extract(hdr.label2_2);
        len_limit.decrement(8w2);

        transition select(hdr.len2.l) { 
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_1 {
        pkt.extract(hdr.label2_1);
        len_limit.decrement(8w1);

        transition postparse_label2;
    }
    
    state preparse_label3 {
        pkt.extract(hdr.len3);
        
        transition select(hdr.len3.l) { 
            16 &&& 16:          parse_label3_16;
            8 &&& 8:          parse_label3_8;
            4 &&& 4:          parse_label3_4;
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
        }
    }

    state postparse_label3 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len4;
            true:                     accept;
        }
    }
    
    state parse_label3_16 {
        pkt.extract(hdr.label3_16);
        len_limit.decrement(8w16);

        transition select(hdr.len3.l) { 
            8 &&& 8:          parse_label3_8;
            4 &&& 4:          parse_label3_4;
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_8 {
        pkt.extract(hdr.label3_8);
        len_limit.decrement(8w8);

        transition select(hdr.len3.l) { 
            4 &&& 4:          parse_label3_4;
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_4 {
        pkt.extract(hdr.label3_4);
        len_limit.decrement(8w4);

        transition select(hdr.len3.l) { 
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_2 {
        pkt.extract(hdr.label3_2);
        len_limit.decrement(8w2);

        transition select(hdr.len3.l) { 
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_1 {
        pkt.extract(hdr.label3_1);
        len_limit.decrement(8w1);

        transition postparse_label3;
    }
    
    state preparse_label4 {
        pkt.extract(hdr.len4);
        
        transition select(hdr.len4.l) { 
            16 &&& 16:          parse_label4_16;
            8 &&& 8:          parse_label4_8;
            4 &&& 4:          parse_label4_4;
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
        }
    }

    state postparse_label4 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len5;
            true:                     accept;
        }
    }
    
    state parse_label4_16 {
        pkt.extract(hdr.label4_16);
        len_limit.decrement(8w16);

        transition select(hdr.len4.l) { 
            8 &&& 8:          parse_label4_8;
            4 &&& 4:          parse_label4_4;
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_8 {
        pkt.extract(hdr.label4_8);
        len_limit.decrement(8w8);

        transition select(hdr.len4.l) { 
            4 &&& 4:          parse_label4_4;
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_4 {
        pkt.extract(hdr.label4_4);
        len_limit.decrement(8w4);

        transition select(hdr.len4.l) { 
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_2 {
        pkt.extract(hdr.label4_2);
        len_limit.decrement(8w2);

        transition select(hdr.len4.l) { 
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_1 {
        pkt.extract(hdr.label4_1);
        len_limit.decrement(8w1);

        transition postparse_label4;
    }
    
    state preparse_label5 {
        pkt.extract(hdr.len5);
        
        transition select(hdr.len5.l) { 
            16 &&& 16:          parse_label5_16;
            8 &&& 8:          parse_label5_8;
            4 &&& 4:          parse_label5_4;
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
        }
    }

    state postparse_label5 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len6;
            true:                     accept;
        }
    }
    
    state parse_label5_16 {
        pkt.extract(hdr.label5_16);
        len_limit.decrement(8w16);

        transition select(hdr.len5.l) { 
            8 &&& 8:          parse_label5_8;
            4 &&& 4:          parse_label5_4;
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_8 {
        pkt.extract(hdr.label5_8);
        len_limit.decrement(8w8);

        transition select(hdr.len5.l) { 
            4 &&& 4:          parse_label5_4;
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_4 {
        pkt.extract(hdr.label5_4);
        len_limit.decrement(8w4);

        transition select(hdr.len5.l) { 
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_2 {
        pkt.extract(hdr.label5_2);
        len_limit.decrement(8w2);

        transition select(hdr.len5.l) { 
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_1 {
        pkt.extract(hdr.label5_1);
        len_limit.decrement(8w1);

        transition postparse_label5;
    }
    
    state preparse_label6 {
        pkt.extract(hdr.len6);
        
        transition select(hdr.len6.l) { 
            16 &&& 16:          parse_label6_16;
            8 &&& 8:          parse_label6_8;
            4 &&& 4:          parse_label6_4;
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
        }
    }

    state postparse_label6 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len7;
            true:                     accept;
        }
    }
    
    state parse_label6_16 {
        pkt.extract(hdr.label6_16);
        len_limit.decrement(8w16);

        transition select(hdr.len6.l) { 
            8 &&& 8:          parse_label6_8;
            4 &&& 4:          parse_label6_4;
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_8 {
        pkt.extract(hdr.label6_8);
        len_limit.decrement(8w8);

        transition select(hdr.len6.l) { 
            4 &&& 4:          parse_label6_4;
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_4 {
        pkt.extract(hdr.label6_4);
        len_limit.decrement(8w4);

        transition select(hdr.len6.l) { 
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_2 {
        pkt.extract(hdr.label6_2);
        len_limit.decrement(8w2);

        transition select(hdr.len6.l) { 
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_1 {
        pkt.extract(hdr.label6_1);
        len_limit.decrement(8w1);

        transition postparse_label6;
    }
    

    state postparse_dns {
        pkt.extract(hdr.dns_extra);

        transition select(hdr.dns.ancount) {
            1:                          parse_answer;
            default:                    accept;
        }
    }

    state parse_answer {
        pkt.extract(hdr.dns_answer);
        pkt.extract(hdr.dns_answer_ip);

        transition accept;
    }




}



/*************************************************************************
*********************** I N G - D E P A R S E R  *************************
*************************************************************************/


control SwitchIngressDeparser(
    packet_out pkt,
    inout headers hdr,
    in ig_metadata ig_md,
    in ingress_intrinsic_metadata_for_deparser_t ig_intr_dprsr_md
) {

    apply {
        pkt.emit(hdr.ethernet);
        pkt.emit(hdr.ipv4);
        pkt.emit(hdr.ipv4v);
        pkt.emit(hdr.ipv4v_alt);
        pkt.emit(hdr.udp_p);
        pkt.emit(hdr.udp_p_alt);
        pkt.emit(hdr.udp);
        pkt.emit(hdr.dns);

        
        
        pkt.emit(hdr.len6);
        
        pkt.emit(hdr.label6_16);
        pkt.emit(hdr.label6_8);
        pkt.emit(hdr.label6_4);
        pkt.emit(hdr.label6_2);
        pkt.emit(hdr.label6_1);
        pkt.emit(hdr.len5);
        
        pkt.emit(hdr.label5_16);
        pkt.emit(hdr.label5_8);
        pkt.emit(hdr.label5_4);
        pkt.emit(hdr.label5_2);
        pkt.emit(hdr.label5_1);
        pkt.emit(hdr.len4);
        
        pkt.emit(hdr.label4_16);
        pkt.emit(hdr.label4_8);
        pkt.emit(hdr.label4_4);
        pkt.emit(hdr.label4_2);
        pkt.emit(hdr.label4_1);
        pkt.emit(hdr.len3);
        
        pkt.emit(hdr.label3_16);
        pkt.emit(hdr.label3_8);
        pkt.emit(hdr.label3_4);
        pkt.emit(hdr.label3_2);
        pkt.emit(hdr.label3_1);
        pkt.emit(hdr.len2);
        
        pkt.emit(hdr.label2_16);
        pkt.emit(hdr.label2_8);
        pkt.emit(hdr.label2_4);
        pkt.emit(hdr.label2_2);
        pkt.emit(hdr.label2_1);
        pkt.emit(hdr.len1);
        
        pkt.emit(hdr.label1_16);
        pkt.emit(hdr.label1_8);
        pkt.emit(hdr.label1_4);
        pkt.emit(hdr.label1_2);
        pkt.emit(hdr.label1_1);
        pkt.emit(hdr.dns_extra);
        pkt.emit(hdr.dns_answer);
        pkt.emit(hdr.dns_answer_ip);
    }
}



/*************************************************************************
**************************** I N G R E S S  ******************************
*************************************************************************/



control SwitchIngress(
    inout headers hdr,
    inout ig_metadata ig_md,
    in ingress_intrinsic_metadata_t ig_intr_md,
    in ingress_intrinsic_metadata_from_parser_t ig_intr_prsr_md,
    inout ingress_intrinsic_metadata_for_deparser_t ig_intr_dprsr_md,
    inout ingress_intrinsic_metadata_for_tm_t ig_intr_tm_md
) {

    action hit(PortId_t port) {
        ig_intr_tm_md.ucast_egress_port = port;
    }

    action miss() {
        ig_intr_dprsr_md.drop_ctl = 0x1; // Drop packet.
    }

    table ipv4_forward {
        key = {
            hdr.ipv4v.ip_d : lpm;
        }

        actions = {
            hit;
            miss;
        }

        const default_action = hit(1);
        size = 1024;
    }

    apply {
        if (hdr.ipv4v.isValid()) {
            // Basic IP routing
            ipv4_forward.apply();
        }
    }
}



/*************************************************************************
*********************** E N G - P A R S E R  *****************************
*************************************************************************/

parser SwitchEgressParser(
    packet_in pkt,
    out headers hdr,
    out eg_metadata meta,
    out egress_intrinsic_metadata_t eg_intr_md
) {
    ParserCounter()         len_limit;

    state start {
        pkt.extract(eg_intr_md);
        transition parse_ethernet;        
    }

    
    state parse_ethernet {
        pkt.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            ETHERTYPE_IPV4 :    parse_ipv4;
            default :           accept;
        }
    }

    state parse_ipv4 {
        pkt.extract(hdr.ipv4);
        pkt.extract(hdr.ipv4v);
        transition select(hdr.ipv4.protocol) {
            UDP_PROTOCOL: parse_udp;
            default :   accept;
        }
    }

    state parse_udp {
        pkt.extract(hdr.udp_p);
        pkt.extract(hdr.udp);
        transition select(hdr.udp_p.dport,hdr.udp_p.sport) {
            (_,53) :      parse_dns_h;
            (53,_) :      parse_dns_h;
            default :   accept;
        }
    }

    state parse_dns_h {
        pkt.extract(hdr.dns);
        len_limit.set(8w60);

        transition parse_len1;
    }

    
    state parse_len1 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label1;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len2 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label2;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len3 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label3;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len4 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label4;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len5 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label5;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len6 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            1 .. 31:           preparse_label6;
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    
    state parse_len7 {
        bit<8> len = pkt.lookahead<bit<8>>();
        len_limit.decrement(8w1);

        transition select(len) {
            
            0:                                      postparse_dns;
            default:                                accept;
        }
    }
    

    
    state preparse_label1 {
        pkt.extract(hdr.len1);
        
        transition select(hdr.len1.l) { 
            16 &&& 16:          parse_label1_16;
            8 &&& 8:          parse_label1_8;
            4 &&& 4:          parse_label1_4;
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
        }
    }

    state postparse_label1 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len2;
            true:                     accept;
        }
    }
    
    state parse_label1_16 {
        pkt.extract(hdr.label1_16);
        len_limit.decrement(8w16);

        transition select(hdr.len1.l) { 
            8 &&& 8:          parse_label1_8;
            4 &&& 4:          parse_label1_4;
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_8 {
        pkt.extract(hdr.label1_8);
        len_limit.decrement(8w8);

        transition select(hdr.len1.l) { 
            4 &&& 4:          parse_label1_4;
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_4 {
        pkt.extract(hdr.label1_4);
        len_limit.decrement(8w4);

        transition select(hdr.len1.l) { 
            2 &&& 2:          parse_label1_2;
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_2 {
        pkt.extract(hdr.label1_2);
        len_limit.decrement(8w2);

        transition select(hdr.len1.l) { 
            1 &&& 1:          parse_label1_1;
            default:                  postparse_label1;
        }
    }
    
    state parse_label1_1 {
        pkt.extract(hdr.label1_1);
        len_limit.decrement(8w1);

        transition postparse_label1;
    }
    
    state preparse_label2 {
        pkt.extract(hdr.len2);
        
        transition select(hdr.len2.l) { 
            16 &&& 16:          parse_label2_16;
            8 &&& 8:          parse_label2_8;
            4 &&& 4:          parse_label2_4;
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
        }
    }

    state postparse_label2 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len3;
            true:                     accept;
        }
    }
    
    state parse_label2_16 {
        pkt.extract(hdr.label2_16);
        len_limit.decrement(8w16);

        transition select(hdr.len2.l) { 
            8 &&& 8:          parse_label2_8;
            4 &&& 4:          parse_label2_4;
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_8 {
        pkt.extract(hdr.label2_8);
        len_limit.decrement(8w8);

        transition select(hdr.len2.l) { 
            4 &&& 4:          parse_label2_4;
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_4 {
        pkt.extract(hdr.label2_4);
        len_limit.decrement(8w4);

        transition select(hdr.len2.l) { 
            2 &&& 2:          parse_label2_2;
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_2 {
        pkt.extract(hdr.label2_2);
        len_limit.decrement(8w2);

        transition select(hdr.len2.l) { 
            1 &&& 1:          parse_label2_1;
            default:                  postparse_label2;
        }
    }
    
    state parse_label2_1 {
        pkt.extract(hdr.label2_1);
        len_limit.decrement(8w1);

        transition postparse_label2;
    }
    
    state preparse_label3 {
        pkt.extract(hdr.len3);
        
        transition select(hdr.len3.l) { 
            16 &&& 16:          parse_label3_16;
            8 &&& 8:          parse_label3_8;
            4 &&& 4:          parse_label3_4;
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
        }
    }

    state postparse_label3 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len4;
            true:                     accept;
        }
    }
    
    state parse_label3_16 {
        pkt.extract(hdr.label3_16);
        len_limit.decrement(8w16);

        transition select(hdr.len3.l) { 
            8 &&& 8:          parse_label3_8;
            4 &&& 4:          parse_label3_4;
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_8 {
        pkt.extract(hdr.label3_8);
        len_limit.decrement(8w8);

        transition select(hdr.len3.l) { 
            4 &&& 4:          parse_label3_4;
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_4 {
        pkt.extract(hdr.label3_4);
        len_limit.decrement(8w4);

        transition select(hdr.len3.l) { 
            2 &&& 2:          parse_label3_2;
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_2 {
        pkt.extract(hdr.label3_2);
        len_limit.decrement(8w2);

        transition select(hdr.len3.l) { 
            1 &&& 1:          parse_label3_1;
            default:                  postparse_label3;
        }
    }
    
    state parse_label3_1 {
        pkt.extract(hdr.label3_1);
        len_limit.decrement(8w1);

        transition postparse_label3;
    }
    
    state preparse_label4 {
        pkt.extract(hdr.len4);
        
        transition select(hdr.len4.l) { 
            16 &&& 16:          parse_label4_16;
            8 &&& 8:          parse_label4_8;
            4 &&& 4:          parse_label4_4;
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
        }
    }

    state postparse_label4 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len5;
            true:                     accept;
        }
    }
    
    state parse_label4_16 {
        pkt.extract(hdr.label4_16);
        len_limit.decrement(8w16);

        transition select(hdr.len4.l) { 
            8 &&& 8:          parse_label4_8;
            4 &&& 4:          parse_label4_4;
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_8 {
        pkt.extract(hdr.label4_8);
        len_limit.decrement(8w8);

        transition select(hdr.len4.l) { 
            4 &&& 4:          parse_label4_4;
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_4 {
        pkt.extract(hdr.label4_4);
        len_limit.decrement(8w4);

        transition select(hdr.len4.l) { 
            2 &&& 2:          parse_label4_2;
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_2 {
        pkt.extract(hdr.label4_2);
        len_limit.decrement(8w2);

        transition select(hdr.len4.l) { 
            1 &&& 1:          parse_label4_1;
            default:                  postparse_label4;
        }
    }
    
    state parse_label4_1 {
        pkt.extract(hdr.label4_1);
        len_limit.decrement(8w1);

        transition postparse_label4;
    }
    
    state preparse_label5 {
        pkt.extract(hdr.len5);
        
        transition select(hdr.len5.l) { 
            16 &&& 16:          parse_label5_16;
            8 &&& 8:          parse_label5_8;
            4 &&& 4:          parse_label5_4;
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
        }
    }

    state postparse_label5 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len6;
            true:                     accept;
        }
    }
    
    state parse_label5_16 {
        pkt.extract(hdr.label5_16);
        len_limit.decrement(8w16);

        transition select(hdr.len5.l) { 
            8 &&& 8:          parse_label5_8;
            4 &&& 4:          parse_label5_4;
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_8 {
        pkt.extract(hdr.label5_8);
        len_limit.decrement(8w8);

        transition select(hdr.len5.l) { 
            4 &&& 4:          parse_label5_4;
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_4 {
        pkt.extract(hdr.label5_4);
        len_limit.decrement(8w4);

        transition select(hdr.len5.l) { 
            2 &&& 2:          parse_label5_2;
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_2 {
        pkt.extract(hdr.label5_2);
        len_limit.decrement(8w2);

        transition select(hdr.len5.l) { 
            1 &&& 1:          parse_label5_1;
            default:                  postparse_label5;
        }
    }
    
    state parse_label5_1 {
        pkt.extract(hdr.label5_1);
        len_limit.decrement(8w1);

        transition postparse_label5;
    }
    
    state preparse_label6 {
        pkt.extract(hdr.len6);
        
        transition select(hdr.len6.l) { 
            16 &&& 16:          parse_label6_16;
            8 &&& 8:          parse_label6_8;
            4 &&& 4:          parse_label6_4;
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
        }
    }

    state postparse_label6 {
        transition select(len_limit.is_negative()) {
            false:                    parse_len7;
            true:                     accept;
        }
    }
    
    state parse_label6_16 {
        pkt.extract(hdr.label6_16);
        len_limit.decrement(8w16);

        transition select(hdr.len6.l) { 
            8 &&& 8:          parse_label6_8;
            4 &&& 4:          parse_label6_4;
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_8 {
        pkt.extract(hdr.label6_8);
        len_limit.decrement(8w8);

        transition select(hdr.len6.l) { 
            4 &&& 4:          parse_label6_4;
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_4 {
        pkt.extract(hdr.label6_4);
        len_limit.decrement(8w4);

        transition select(hdr.len6.l) { 
            2 &&& 2:          parse_label6_2;
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_2 {
        pkt.extract(hdr.label6_2);
        len_limit.decrement(8w2);

        transition select(hdr.len6.l) { 
            1 &&& 1:          parse_label6_1;
            default:                  postparse_label6;
        }
    }
    
    state parse_label6_1 {
        pkt.extract(hdr.label6_1);
        len_limit.decrement(8w1);

        transition postparse_label6;
    }
    

    state postparse_dns {
        pkt.extract(hdr.dns_extra);

        transition select(hdr.dns.ancount) {
            1:                          parse_answer;
            default:                    accept;
        }
    }

    state parse_answer {
        pkt.extract(hdr.dns_answer);
        pkt.extract(hdr.dns_answer_ip);

        transition accept;
    }



}


/*************************************************************************
*********************** E N G - D E P A R S E R  *************************
*************************************************************************/


control SwitchEgressDeparser(
    packet_out pkt,
    inout headers hdr,
    in eg_metadata eg_md,
    in egress_intrinsic_metadata_for_deparser_t eg_intr_md_for_dprsr
) {
    apply {
        pkt.emit(hdr.ethernet);
        pkt.emit(hdr.ipv4);
        pkt.emit(hdr.ipv4v);
        pkt.emit(hdr.ipv4v_alt);
        pkt.emit(hdr.udp_p);
        pkt.emit(hdr.udp_p_alt);
        pkt.emit(hdr.udp);
        pkt.emit(hdr.dns);

        
        pkt.emit(hdr.len6);
        
        pkt.emit(hdr.label6_16);
        pkt.emit(hdr.label6_8);
        pkt.emit(hdr.label6_4);
        pkt.emit(hdr.label6_2);
        pkt.emit(hdr.label6_1);
        pkt.emit(hdr.len5);
        
        pkt.emit(hdr.label5_16);
        pkt.emit(hdr.label5_8);
        pkt.emit(hdr.label5_4);
        pkt.emit(hdr.label5_2);
        pkt.emit(hdr.label5_1);
        pkt.emit(hdr.len4);
        
        pkt.emit(hdr.label4_16);
        pkt.emit(hdr.label4_8);
        pkt.emit(hdr.label4_4);
        pkt.emit(hdr.label4_2);
        pkt.emit(hdr.label4_1);
        pkt.emit(hdr.len3);
        
        pkt.emit(hdr.label3_16);
        pkt.emit(hdr.label3_8);
        pkt.emit(hdr.label3_4);
        pkt.emit(hdr.label3_2);
        pkt.emit(hdr.label3_1);
        pkt.emit(hdr.len2);
        
        pkt.emit(hdr.label2_16);
        pkt.emit(hdr.label2_8);
        pkt.emit(hdr.label2_4);
        pkt.emit(hdr.label2_2);
        pkt.emit(hdr.label2_1);
        pkt.emit(hdr.len1);
        
        pkt.emit(hdr.label1_16);
        pkt.emit(hdr.label1_8);
        pkt.emit(hdr.label1_4);
        pkt.emit(hdr.label1_2);
        pkt.emit(hdr.label1_1);
        pkt.emit(hdr.dns_extra);
        pkt.emit(hdr.dns_answer);
        pkt.emit(hdr.dns_answer_ip);
    }
}


/*************************************************************************
**************************** E G R E S S *********************************
*************************************************************************/

control SwitchEgress(
    inout headers hdr,
    inout eg_metadata meta,
    in egress_intrinsic_metadata_t eg_intr_md,
    in egress_intrinsic_metadata_from_parser_t eg_intr_md_from_prsr,
    inout egress_intrinsic_metadata_for_deparser_t ig_intr_dprs_md,
    inout egress_intrinsic_metadata_for_output_port_t eg_intr_oport_md
) {
    
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    domain_hash_func_2;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    domain_hash_func_2_0;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    subdomain_hash_func_2;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    subdomain_hash_func_2_0;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    domain_hash_func_3;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    domain_hash_func_3_0;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    subdomain_hash_func_3;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    subdomain_hash_func_3_0;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    domain_hash_func_4;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    domain_hash_func_4_0;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    subdomain_hash_func_4;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    subdomain_hash_func_4_0;
    Hash<bit<32>>(HashAlgorithm_t.CRC32)    sig_hash_func;
    Hash<bit<16>>(HashAlgorithm_t.CRC16)    idx_hash_func;
    
    
    bit<248>        subdomain_first_label = 0;
    
    bit<8>                                  cached_time = 0;
    bit<32>                                 cached_ip = 0;
    bit<32>                                 current_time = 0;
    bit<32>                                 domain_hash;
    bit<32>                                 subdomain_hash = 0;
    bit<32>                                 dns_sig = 0;
    bit<16>                                 idx_hash = 0;
    bit<8>                                  labels_count_domain;
    
    bool                                    domain_labels_2 = false;
    bool                                    domain_labels_3 = false;
    
    bool                                    cachable = false;
    bool                                    is_answer = false;

    
    action dns_split_labels_action_2() {
        domain_labels_2 = true;
    }
    action dns_split_labels_action_3() {
        domain_labels_3 = true;
    }

    action drop() {
        ig_intr_dprs_md.drop_ctl = 0x1;
    }

    
    table dns_domain_parts_2 {
        key = { 
            hdr.label1_16.l: exact;
            hdr.label2_16.l: exact;
            hdr.label1_8.l: exact;
            hdr.label2_8.l: exact;
            hdr.label1_4.l: exact;
            hdr.label2_4.l: exact;
            hdr.label1_2.l: exact;
            hdr.label2_2.l: exact;
            hdr.label1_1.l: exact;
            hdr.label2_1.l: exact;
        }
        actions = {
            dns_split_labels_action_2;
            NoAction;            
        }
        size = 8192;
        default_action = NoAction;
    }
    
    table dns_domain_parts_3 {
        key = { 
            hdr.label1_16.l: exact;
            hdr.label2_16.l: exact;
            hdr.label3_16.l: exact;
            hdr.label1_8.l: exact;
            hdr.label2_8.l: exact;
            hdr.label3_8.l: exact;
            hdr.label1_4.l: exact;
            hdr.label2_4.l: exact;
            hdr.label3_4.l: exact;
            hdr.label1_2.l: exact;
            hdr.label2_2.l: exact;
            hdr.label3_2.l: exact;
            hdr.label1_1.l: exact;
            hdr.label2_1.l: exact;
            hdr.label3_1.l: exact;
        }
        actions = {
            dns_split_labels_action_3;
            NoAction;            
        }
        size = 4096;
        default_action = NoAction;
    }
    

    
    action calc_subdomain_4() {
         subdomain_hash = subdomain_hash_func_4.get({  hdr.label6_8.l,  hdr.label6_4.l,  hdr.label6_2.l,  hdr.label6_1.l,  hdr.label5_8.l,  hdr.label5_4.l,  hdr.label5_2.l,  hdr.label5_1.l  });
    }
    
    action calc_subdomain_3() {
         subdomain_hash = subdomain_hash_func_3.get({  hdr.label6_8.l,  hdr.label6_4.l,  hdr.label6_2.l,  hdr.label6_1.l,  hdr.label5_8.l,  hdr.label5_4.l,  hdr.label5_2.l,  hdr.label5_1.l,  hdr.label4_8.l,  hdr.label4_4.l,  hdr.label4_2.l,  hdr.label4_1.l  });
    }
    
    action calc_subdomain_2() {
         subdomain_hash = subdomain_hash_func_2.get({  hdr.label6_8.l,  hdr.label6_4.l,  hdr.label6_2.l,  hdr.label6_1.l,  hdr.label5_8.l,  hdr.label5_4.l,  hdr.label5_2.l,  hdr.label5_1.l,  hdr.label4_8.l,  hdr.label4_4.l,  hdr.label4_2.l,  hdr.label4_1.l,  hdr.label3_8.l,  hdr.label3_4.l,  hdr.label3_2.l,  hdr.label3_1.l  });
    }
    

    
    action calc_domain_4() {
        domain_hash = domain_hash_func_4.get({  hdr.label4_8.l, hdr.label4_4.l, hdr.label4_2.l, hdr.label4_1.l, hdr.label3_8.l, hdr.label3_4.l, hdr.label3_2.l, hdr.label3_1.l, hdr.label2_8.l, hdr.label2_4.l, hdr.label2_2.l, hdr.label2_1.l, hdr.label1_8.l, hdr.label1_4.l, hdr.label1_2.l, hdr.label1_1.l });
    }
    
    action calc_domain_3() {
        domain_hash = domain_hash_func_3.get({  hdr.label3_8.l, hdr.label3_4.l, hdr.label3_2.l, hdr.label3_1.l, hdr.label2_8.l, hdr.label2_4.l, hdr.label2_2.l, hdr.label2_1.l, hdr.label1_8.l, hdr.label1_4.l, hdr.label1_2.l, hdr.label1_1.l });
    }
    
    action calc_domain_2() {
        domain_hash = domain_hash_func_2.get({  hdr.label2_8.l, hdr.label2_4.l, hdr.label2_2.l, hdr.label2_1.l, hdr.label1_8.l, hdr.label1_4.l, hdr.label1_2.l, hdr.label1_1.l });
    }
    

    
    
    action calc_domain_4_extra() {
        domain_hash = domain_hash + domain_hash_func_4_0.get({  hdr.label4_16.l, hdr.label3_16.l, hdr.label2_16.l, hdr.label1_16.l });
    }
    
    action calc_subdomain_4_extra() {
        subdomain_hash = subdomain_hash + subdomain_hash_func_4_0.get({  hdr.label6_16.l,  hdr.label5_16.l  });
    }

    
    action calc_next_subdomain_label_4() {
        subdomain_first_label =  hdr.label5_16.l ++  hdr.label5_8.l ++  hdr.label5_4.l ++  hdr.label5_2.l ++  hdr.label5_1.l  ;
    }
    
    
    action calc_domain_3_extra() {
        domain_hash = domain_hash + domain_hash_func_3_0.get({  hdr.label3_16.l, hdr.label2_16.l, hdr.label1_16.l });
    }
    
    action calc_subdomain_3_extra() {
        subdomain_hash = subdomain_hash + subdomain_hash_func_3_0.get({  hdr.label6_16.l,  hdr.label5_16.l,  hdr.label4_16.l  });
    }

    
    action calc_next_subdomain_label_3() {
        subdomain_first_label =  hdr.label4_16.l ++  hdr.label4_8.l ++  hdr.label4_4.l ++  hdr.label4_2.l ++  hdr.label4_1.l  ;
    }
    
    
    action calc_domain_2_extra() {
        domain_hash = domain_hash + domain_hash_func_2_0.get({  hdr.label2_16.l, hdr.label1_16.l });
    }
    
    action calc_subdomain_2_extra() {
        subdomain_hash = subdomain_hash + subdomain_hash_func_2_0.get({  hdr.label6_16.l,  hdr.label5_16.l,  hdr.label4_16.l,  hdr.label3_16.l  });
    }

    
    action calc_next_subdomain_label_2() {
        subdomain_first_label =  hdr.label3_16.l ++  hdr.label3_8.l ++  hdr.label3_4.l ++  hdr.label3_2.l ++  hdr.label3_1.l  ;
    }
    

    
    DirectCounter<bit<32>>(CounterType_t.PACKETS) next_subdomain_stats; 
    action count_subdomain_first() {
        next_subdomain_stats.count();
    }

    action nop() {
    }

    table next_subdomain_stats_table {
        key = { subdomain_first_label : exact; }
        actions = { 
            count_subdomain_first;
            @defaultonly nop;
        }
        counters = next_subdomain_stats;
        size = 1024;
    }

    

    
    table dns_is_blocklist {
        key = {
            hdr.dns_extra.isValid(): exact;
            domain_hash: exact;
        }
        actions = {
            drop;
            NoAction;
        }
        size = 256;
        default_action = NoAction();
    }

    
    action mark_cachable() {
        cachable = true;
    }

    action mark_cachable_answer() {
        cachable = true;
        is_answer = true;
    }

    table dns_is_cachable_answer {
        key = {
            hdr.dns.qr: exact;
            hdr.dns_answer.pointer: exact;
            hdr.dns_answer.type: exact;
            hdr.dns_answer.clas: exact;
            hdr.dns_answer.rdlength: exact;
        }
        actions = {
            NoAction;
            mark_cachable_answer;
        }
        size = 1;
        default_action = NoAction();
        const entries = {
            (1, CACHE_PREFIX, 1, 1, 4): mark_cachable_answer();
        }
    }

    table dns_is_cachable_question {
        key = {
            hdr.dns.qr: exact;
            hdr.dns_extra.qtype: exact;
            hdr.dns_extra.qclass: exact;
        }
        actions = {
            mark_cachable;
            NoAction;
        }
        size = 1;
        default_action = NoAction();
        const entries = {
            (0, 1, 1): mark_cachable();
        }
    }

    Register<dns_cache_sig, _>(65536)                         sig_cache_register;
    RegisterAction<_, _, bit<8>>(sig_cache_register)          validate_time = {
        void apply(inout dns_cache_sig value, out bit<8> res){
            if (value.ts > current_time && dns_sig == value.sig) {
                res = 1;
            } else {
                res = 0;
            }
        }
    };

    action exec_validate_time(bit<16> idx){
        cached_time = validate_time.execute(idx);
    }

    RegisterAction<_, _, void>(sig_cache_register)         write_time = {
        void apply(inout dns_cache_sig value){
            value.ts = current_time;
            value.sig = dns_sig;
        }
    };

    action exec_write_time(bit<16> idx){
        write_time.execute(idx);
    }

    Register<bit<32>, _>(65536)             ip_cache_register;
    RegisterAction<_, _, bit<32>>(ip_cache_register)    read_cache_value = {
        void apply(inout bit<32> value, out bit<32> res){
            res = value;
        }
    };

    action exec_read_cache_value(bit<16> idx){
        cached_ip = read_cache_value.execute(idx);
    }

    RegisterAction<_, _, void>(ip_cache_register)         write_cache_value = {
        void apply(inout bit<32> value){
            value = hdr.dns_answer_ip.res;
        }
    };

    action exec_write_cache_value(bit<16> idx){
        write_cache_value.execute(idx);
    }

    action add_ttl() {
        current_time = current_time + hdr.dns_answer.ttl; 
    }


    apply {

        
        dns_domain_parts_3.apply();
        dns_domain_parts_2.apply();

        
        if (domain_labels_3 == true) {
            calc_domain_4();
            calc_subdomain_4();
            
            calc_domain_4_extra();
            calc_subdomain_4_extra();
        }
        else if (domain_labels_2 == true) {
            calc_domain_3();
            calc_subdomain_3();
            
            calc_domain_3_extra();
            calc_subdomain_3_extra();
        } else {
            calc_domain_2();
            calc_subdomain_2();
            
            calc_domain_2_extra();
            calc_subdomain_2_extra();
        }

        
        dns_is_blocklist.apply(); // check domain blocklisted

        
        idx_hash = idx_hash_func.get({domain_hash, subdomain_hash});
        dns_is_cachable_answer.apply();
        dns_is_cachable_question.apply();

        
        if (domain_labels_3 == true) {
            calc_next_subdomain_label_4();
        }
        else if (domain_labels_2 == true) {
            calc_next_subdomain_label_3();
        } else {
            calc_next_subdomain_label_2();
        }

        next_subdomain_stats_table.apply();
        

        
        if (hdr.dns_extra.isValid() && cachable && hdr.dns.ancount <= 1 && ig_intr_dprs_md.drop_ctl != 0x1) { 
            dns_sig = sig_hash_func.get({  hdr.label3_16.l,  hdr.label3_8.l,  hdr.label3_4.l,  hdr.label3_2.l,  hdr.label3_1.l,  hdr.label1_16.l,  hdr.label1_8.l,  hdr.label1_4.l,  hdr.label1_2.l,  hdr.label1_1.l,  domain_hash, subdomain_hash });        
    
            current_time = eg_intr_md_from_prsr.global_tstamp[47:16] >> 13;

            if (is_answer) {
                add_ttl();
            }

            if (is_answer) {
                exec_write_time(idx_hash);
            } else {
                exec_validate_time(idx_hash);
            }

            if (is_answer) {
                exec_write_cache_value(idx_hash);
            } else {
                exec_read_cache_value(idx_hash);
            }
            
            if (cached_time == 1) {
                hdr.ipv4.totalLen = hdr.ipv4.totalLen + 16;
                hdr.ipv4.hdrChecksum = hdr.ipv4.hdrChecksum - 16;
                hdr.ipv4v_alt.ip_s = hdr.ipv4v.ip_d;
                hdr.ipv4v_alt.ip_d = hdr.ipv4v.ip_s;
                hdr.ipv4v_alt.setValid();
                hdr.ipv4v.setInvalid();
                hdr.udp_p_alt.sport = hdr.udp_p.dport;
                hdr.udp_p_alt.dport = hdr.udp_p.sport;
                hdr.udp_p_alt.setValid();
                hdr.udp_p.setInvalid();
                hdr.udp.chksum = 0;
                hdr.udp.len = hdr.udp.len + 16;
                hdr.dns.qr = 1w1;
                hdr.dns.ancount = 16w1;
                hdr.dns_answer.pointer = CACHE_PREFIX;
                hdr.dns_answer.type = 16w1;
                hdr.dns_answer.clas = 16w1;
                hdr.dns_answer.ttl = 32w1;
                hdr.dns_answer.rdlength = 16w4;
                hdr.dns_answer.setValid();
                hdr.dns_answer_ip.res = cached_ip;
                hdr.dns_answer_ip.setValid();
            }
        }
        
    }
}

/*************************************************************************
****************************** S E T U P *********************************
*************************************************************************/

Pipeline(
    SwitchIngressParser(),
    SwitchIngress(),
    SwitchIngressDeparser(),
    SwitchEgressParser(),
    SwitchEgress(),
    SwitchEgressDeparser()
) pipe;

Switch(pipe) main;