import math
import json
from colorama import init, Fore, Back, Style
from jinja2 import Template, Environment, FileSystemLoader

compile_configuration = {
    "max_domain_labels": 4, # Maximum domain labels to match against for domain extraction
    "max_labels": 6, # Maximum number of labels in DNS query to parse
    "max_bytes_per_label": 31, # Maximum bytes (characters) can be parsed for a single label in DNS query
    "max_bytes_total": 60, # Maximum total number of bytes of a DNS query to be parsed

    "domain_blocklist": True, # Enable domain blocklist feature
    "subdomain_allowlist": False, # Enable subdomain allowlist feature
    "subdomain_stats": False, # Enable subdomain counter feature
    "subdomain_next_stats": True, # Enable first subdomain label counter feature
    "cache_answer": True, # Enable dns answer cache feature
}


def previous_power_of_2(x):
    return 0 if x == 0 else math.floor(math.log2(x))


def print_dict(title, d, color=Fore.CYAN):
    print(color + "========== " + title + " ==========" + Style.RESET_ALL)
    print(json.dumps(d, indent=2))


def create_template(configuration, source, target=None):
    if configuration["max_domain_labels"] >= configuration["max_labels"]:
        raise Exception("Can't compile if max_labels is too close to max_tld_labels")

    configuration["max_tld_labels"] = configuration["max_domain_labels"] - 1
    configuration["max_bytes_powers"] = [
        i
        for i in range(1, configuration["max_bytes_per_label"])
        if math.log2(i).is_integer()
    ]
    configuration["max_bytes_powers_8"] = [
        i for i in configuration["max_bytes_powers"] if i < 16
    ]
    configuration["max_bytes_powers_8_not"] = [
        i for i in configuration["max_bytes_powers"] if i >= 16
    ]

    print_dict("Runtime Conf", configuration)

    env = Environment(
        loader=FileSystemLoader("."),
    )

    template = env.get_template(source)

    if not target:
        target = source.replace("template", "")

    with open(target, "w+") as f:
        f.write(template.render(configuration))

    return target


def template():
    gen_path = create_template(
        compile_configuration, "./dns.parser.p4template", "./dns.p4"
    )
    print(f"Template generated to {gen_path}")


if __name__ == "__main__":
    template()
