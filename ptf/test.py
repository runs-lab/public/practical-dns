import logging
import math
import requests
import idna
import sys
import zlib
import zipfile
from io import BytesIO
from time import sleep
from logging import handlers
from crccheck.crc import CrcArc
from ptf import config
from scapy.all import Ether,IP,UDP,DNS,DNSQR,DNSRR,dns_compress
import ptf.testutils as testutils
from bfruntime_client_base_tests import BfRuntimeTest
import bfrt_grpc.client as gc


import sys
from pathlib import Path # if you haven't already done so
file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

# Additionally remove the current file's directory from sys.path
try:
    sys.path.remove(str(parent))
except ValueError: # Already removed
    pass
from template import compile_configuration

p4_program_name = "dns"


def setup_logger(level):
    logger = logging.getLogger('dns-test')
    logger.setLevel(level)
    format = logging.Formatter("[%(asctime)s] [%(levelname)s] %(message)s")

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(format)
    logger.addHandler(ch)

    fh = handlers.RotatingFileHandler('dns-test.log', maxBytes=(1048576*5), backupCount=7)
    fh.setFormatter(format)
    logger.addHandler(fh)
    return logger


############### CONFIG ##############
sw_src_port = 3
sw_dst_port = 4
sw_ip_1 = '10.10.20.1'
sw_ip_2 = '10.10.20.2'
logger = setup_logger(logging.INFO)
#####################################



swports = []
for device, port, ifname in config["interfaces"]:
    swports.append(port)
    swports.sort()
print(swports)

def get_psl():
  r = requests.get('https://raw.githubusercontent.com/publicsuffix/list/master/public_suffix_list.dat')
  return [
    line for line in r.text.split('\n') 
    if len(line) and not (line.startswith('//') or line.startswith('!'))
  ]

def hex2bytes(h):
    startrange = 2 if h.startswith('0x') else 0
    bts = [bytes([int('0x'+h[i:i+2],16)]) for i in range(startrange, len(h), 2)]
    return b''.join(bts).rjust(4,b'\0')

# Closest power of 2
def cl_p2(n):
    lg2 = int(math.log2(n))
    return 2 ** lg2

def crc(domain, variant='16', reverse = False):
    max_bytes = compile_configuration['max_bytes_per_label']
    closest_power2 = cl_p2(max_bytes)
    sum_reg = b''
    sum_16 = b''
    splitdomains = reversed(domain.split('.')) if reverse else domain.split('.')
    for label in splitdomains:
        i = closest_power2
        temp_label = label
        while i >= 1:
            if len(temp_label) >= i:
                if i >= 16:
                    sum_16 += temp_label[:i].encode('ascii')   
                else:
                    sum_reg += temp_label[:i].encode('ascii')   
                temp_label = temp_label[i:]
            else:
                if i >= 16:
                    sum_16 += (0).to_bytes(i, byteorder='big')
                else:
                    sum_reg += (0).to_bytes(i, byteorder='big')
            i = int(i/2)
    total = (CrcArc.calc(sum_reg) if variant == '16' else zlib.crc32(sum_reg))
    if max_bytes >= 16:
        return (total + (CrcArc.calc(sum_16) if variant == '16' else zlib.crc32(sum_16))) % (2 ** int(variant))
    return total

def label2int(label):
    max_bytes = compile_configuration['max_bytes_per_label']
    i = cl_p2(max_bytes) 
    temp_label = label
    bins = ''
    while i >= 1:
        if len(temp_label) >= i:
            bins += "".join(f"{ord(i):08b}" for i in temp_label[:i])
            temp_label = temp_label[i:]
        else: 
            bins += '0' * i * 8
        i = int(i/2)
    return int(bins, 2)

class DNSTest(BfRuntimeTest):
    """@brief Test DNS parsing and analysis
    """

    def setUp(self):
        client_id = 0
        BfRuntimeTest.setUp(self, client_id, p4_program_name)
        self.total_skipped_psl = 0


    def get_all_tables(self):
        self.ipv4_forward = self.bfrt_info.table_get("SwitchIngress.ipv4_forward")
        self.ipv4_forward.entry_del(self.target)
        self.dns_domain_parts_2 = self.bfrt_info.table_get("SwitchEgress.dns_domain_parts_2")
        self.dns_domain_parts_2.entry_del(self.target)
        self.dns_domain_parts_3 = self.bfrt_info.table_get("SwitchEgress.dns_domain_parts_3")
        self.dns_domain_parts_3.entry_del(self.target)
        # self.dns_is_allowlist = self.bfrt_info.table_get("SwitchEgress.dns_is_allowlist")
        # self.dns_is_allowlist.entry_del(self.target)
        self.dns_is_blocklist = self.bfrt_info.table_get("SwitchEgress.dns_is_blocklist")
        self.dns_is_blocklist.entry_del(self.target)
        self.next_subdomain_stats_table = self.bfrt_info.table_get("SwitchEgress.next_subdomain_stats_table")
        self.next_subdomain_stats_table.entry_del(self.target)


    def add_domain_parts(self, int_domain):
        if int_domain.startswith('*'):
            int_domain = int_domain[2:]
        domain = idna.encode(int_domain).decode()
        logger.debug('Adding: %s - %s', int_domain, domain)
        labels = domain.split('.')
        labels_count = len(labels) + 1        

        key = []
        for index, label in enumerate(reversed(labels), start=1):
            label_len = len(label)
            label_temp = label
            if label_len > compile_configuration['max_bytes_per_label']:
                self.total_skipped_psl += 1
                logger.debug('Skipped: %s', domain)
                return
            while label_len > 0:
                closest_power2 = cl_p2(len(label_temp))
                label_len -= closest_power2
                label_part = label_temp[:closest_power2]
                label_temp = label_temp[closest_power2:]
                field = f'hdr.label{index}_{closest_power2}.l'
                label_part_int = int(''.join(hex(ord(x)).replace('0x','') for x in label_part), 16)
                key.append(gc.KeyTuple(field, label_part_int))
                logger.debug('Key: %s == %s', field, label_part_int)

        data = []
        logger.debug('Data: %s == %s', 'labels_count', labels_count)
        if len(labels) == 1:
            pass
        elif len(labels) == 2:
            self.dns_domain_parts_2.entry_add(
                self.target,
                [self.dns_domain_parts_2.make_key(key)],
                [self.dns_domain_parts_2.make_data(data, 'SwitchEgress.dns_split_labels_action_2')]
            )
        elif len(labels) == 3:
            self.dns_domain_parts_3.entry_add(
                self.target,
                [self.dns_domain_parts_3.make_key(key)],
                [self.dns_domain_parts_3.make_data(data, 'SwitchEgress.dns_split_labels_action_3')]
            )
        else:
            pass

    def add_counter_entry(self, label):
        labelint = label2int(label)
        logger.debug('Adding counter: %s - %s', label, labelint)
        self.next_subdomain_stats_table.entry_add(
            self.target,
            [self.next_subdomain_stats_table.make_key([gc.KeyTuple('subdomain_first_label', labelint)])],
            [
                self.next_subdomain_stats_table.make_data([
                    gc.DataTuple('$COUNTER_SPEC_PKTS', 0)
                ],
                'SwitchEgress.count_subdomain_first')
            ]
        )

    def add_domain_blocklist(self, domain):
        hsh = crc(domain, variant='32', reverse=False)
        logger.debug('Adding blocklist: %s - %s', domain, hsh)
        self.dns_is_blocklist.entry_add(
            self.target,
            [self.dns_is_blocklist.make_key([gc.KeyTuple('domain_hash', hsh), gc.KeyTuple('hdr.dns_extra.$valid', True)])],
            [self.dns_is_blocklist.make_data([], 'SwitchEgress.drop')]
        )

    def add_routing_entry(self, ip, port):
        logger.debug('Adding routing entry: %s -> %s', ip, str(port))
        self.ipv4_forward.entry_add(
            self.target,
            [self.ipv4_forward.make_key([gc.KeyTuple('hdr.ipv4v.ip_d', gc.ipv4_to_bytes(ip), prefix_len=32)])],
            [self.ipv4_forward.make_data([gc.DataTuple('port', port)], 'SwitchIngress.hit')]
        )

    def read_counter(self, label):
        labelint = label2int(label)


        resp = self.next_subdomain_stats_table.entry_get(self.target,
            [self.next_subdomain_stats_table.make_key([gc.KeyTuple('subdomain_first_label', labelint)])],
            {"from_hw": True},
            self.next_subdomain_stats_table.make_data(
                [gc.DataTuple("$COUNTER_SPEC_PKTS")],
                'SwitchEgress.count_subdomain_first', get=True)
            )
        data_dict = next(resp)[0].to_dict()
        recv_pkts = data_dict["$COUNTER_SPEC_PKTS"]
        return recv_pkts

    def read_register(self, reg, idx):
        tbl = self.bfrt_info.table_get(reg) 
        resp = tbl.entry_get(
            self.target,
            [tbl.make_key([gc.KeyTuple('$REGISTER_INDEX', idx)])],
            {"from_hw": True}
        )
        return next(resp)[0].to_dict()[f'{reg}.f1'][0]

    def runTest(self):
        self.target = gc.Target(device_id=0, pipe_id=0xffff)
        # Get bfrt_info and set it as part of the test
        self.bfrt_info = self.interface.bfrt_info_get(p4_program_name)

        self.get_all_tables()
        psl_list = get_psl()
        for index, tld in enumerate(psl_list):
            logger.debug(index)
            self.add_domain_parts(tld)     
        logger.info('Added tlds: %s, %s skipped', len(psl_list), self.total_skipped_psl)

        blocklsit_domains = ['malicious.co.uk']         
        for index, domain in enumerate(blocklsit_domains):
            logger.debug(index)
            self.add_domain_blocklist(domain)
        logger.info('Added blocklist: %s', len(blocklsit_domains))

        counter_labels = ['', 'www', 'com', 'elb', 'g', 'graph','api', 'l','download', 'cdn', 'ssl', 'a', 'b', 'c']         
        for index, label in enumerate(counter_labels):
            logger.debug(index)
            self.add_counter_entry(label)
        logger.info('Added counters: %s', len(counter_labels))

        # Add routing
        self.add_routing_entry(sw_ip_1, sw_dst_port)
        self.add_routing_entry(sw_ip_2, sw_dst_port)
        
        sleep(1)
        base_pkt_req = Ether()/IP(src=sw_ip_1,dst=sw_ip_2)/UDP(sport=49152, dport=53)
        base_pkt_ans = Ether()/IP(src=sw_ip_2,dst=sw_ip_1)/UDP(sport=53, dport=49152)

        a1 = dns_compress(base_pkt_ans/DNS(qr=1, qd=DNSQR(qname='a.b.cgoogled.com.'), an=DNSRR(rrname='a.b.cgoogled.com.', rdata="123.0.0.1", ttl=300)))
        q1 = base_pkt_req/DNS(qd=DNSQR(qname='a.b.cgoogled.com.', qtype=1, qclass=1))
        a2 = dns_compress(base_pkt_ans/DNS(qr=1, qd=DNSQR(qname='a.b.cgoogle.com.'), an=DNSRR(rrname='a.b.cgoogle.com.', rdata="8.8.8.8", ttl=10)))
        q2 = base_pkt_req/DNS(qd=DNSQR(qname='a.b.cgoogle.com.', qtype=1, qclass=1))
        a3 = dns_compress(base_pkt_ans/DNS(qr=1, qd=DNSQR(qname='a.b.com.'), an=DNSRR(rrname='a.b.com.', rdata="8.8.8.9", ttl=1)))
        q3 = base_pkt_req/DNS(qd=DNSQR(qname='a.b.com.', qtype=1, qclass=1))
        a4 = dns_compress(base_pkt_ans/DNS(qr=1, qd=DNSQR(qname='a.malicious.co.uk.'), an=DNSRR(rrname='a.malicious.co.uk.', rdata="8.8.8.10", ttl=1000)))
        q4 = base_pkt_req/DNS(qd=DNSQR(qname='a.malicious.co.uk.', qtype=1, qclass=1))
 
        # Test 1
        testutils.send_packet(self, sw_src_port, a1)
        testutils.verify_packet(self, a1, sw_dst_port)

        testutils.send_packet(self, sw_src_port, q1)
        (_, _, rcv_pkt, _) = self.dataplane.poll(port_number=sw_dst_port, timeout=2)
        a1.an.ttl = 1
        self.assertTrue(bytes(Ether(rcv_pkt)[DNS]) == bytes(a1[DNS]), "Test 1: cached answer did not work")

        # Test 2
        testutils.send_packet(self, sw_src_port, a2)
        testutils.verify_packet(self, a2, sw_dst_port)

        testutils.send_packet(self, sw_src_port, q2)
        (_, _, rcv_pkt, _) = self.dataplane.poll(port_number=sw_dst_port, timeout=2)
        a2.an.ttl = 1
        self.assertTrue(bytes(Ether(rcv_pkt)[DNS]) == bytes(a2[DNS]), "Test 2: cached answer did not work")
        testutils.send_packet(self, sw_src_port, q1)
        (_, _, rcv_pkt, _) = self.dataplane.poll(port_number=sw_dst_port, timeout=2)
        a1.an.ttl = 1
        self.assertTrue(bytes(Ether(rcv_pkt)[DNS]) == bytes(a1[DNS]), "Test 2: cached answer 2 did not work")


        # Test 3
        testutils.send_packet(self, sw_src_port, a3)
        testutils.verify_packet(self, a3, sw_dst_port)
        sleep(1)
        testutils.send_packet(self, sw_src_port, q3)
        testutils.verify_packet(self, q3, sw_dst_port)

        testutils.send_packet(self, sw_src_port, a4)
        testutils.send_packet(self, sw_src_port, q4)

        testutils.verify_no_other_packets(self)

        # Test 4
        self.assertTrue(self.read_counter('a') == 4, "Test 4: Wrong count on subdomain (a), or had previouis results")
        self.assertTrue(self.read_counter('b') == 5, "Test 4: Wrong count on subdomain (b), or had previouis results")
        self.assertTrue(self.read_counter('c') == 0, "Test 4: Counted non-existent subdomain, or had previouis results")
