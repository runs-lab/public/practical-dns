# Practical Handling of DNS in the Data Plane

This repository contains the implementation of the solution described in the paper titled "Practical Handling of DNS in the Data Plane".

# Compilation

## Code generation

In order to compile the code you will need to set the parameters controlled in `template.py` and generate a `.p4` file to be used for compilation.  To do so you will need `python3`.

The steps to do so:
1. Install the required dependcied using pip - `python3 -m pip install -r requirements.txt`
2. Edit `template.py` dictionary `compile_configuration` to suit your needs.  The default configuration is the one used in the paper, and is described in more detail there.
3. Run the generator - `python3 template.py`


## .p4 Compilation

The code available is meant to be used with the Intel Tofino platform.  Our code has been validated to work using `SDE-9.7.2` and `SDE-9.9.0`.

Please follow the instructions provided with your SDE to compile the `.p4` code.

Depending on your configuration you may need to add the following flag to the compiler:
```
-Xp4c=\"--disable-parse-depth-limit\"
```

Since we use a manual limit on parse depth, enforced in our code, the compiler may think the maximum depth limit could be exceeded when it cannot.

# Testing 

A test script for the PTF framework is provided under the `ptf/` directory.  In order to run it install the requirements using pip and edit the configuration in the code to fit your environment.

The test file also provides an example of table population, which can be used for evaluations.


# Further Use

If you wish to dive deeper into our implementation we recommend compiling the code without the extra features (disable all feature flags), with small values selected for the parameters.  The `.p4` with such options should be much more readable and focused on the core of the implementation.